import React from 'react';
import PropTypes from 'prop-types'

const PokedexTitle = ({PokemonId, PokemonName}) => {
    return(
        <div>
            {`${PokemonId} - ${PokemonName}`}
        </div>
    )
};

PokedexTitle.protoTypes = {
    PokemonId: PropTypes.number.isRequired,
    PokemonName: PropTypes.string.isRequired,
};

export default PokedexTitle;