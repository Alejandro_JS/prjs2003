import React from 'react';
import PokedexTitle from './PokedexTitle';
import PokedexSprite from './PokedexSprite';
import PokedexData from './PokedexData';
import PokedexAbilities from './PokedexAbilities';
import PokedexDescription from './PokedexDescription';
import PokedexId from './PokedexId';

const PokemonSprite = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/155.png'

const PokedexGeneral = () => (
    <div>
        <PokedexTitle PokemonId = {151} PokemonName = {'Cyndaquil'}></PokedexTitle>
        <PokedexSprite PokemonSprite = {PokemonSprite}></PokedexSprite>
        <PokedexData></PokedexData>
        <PokedexAbilities></PokedexAbilities>
        <PokedexDescription></PokedexDescription>
        <PokedexId></PokedexId>
    </div>
);

export default PokedexGeneral;