import React from 'react';
import PropTypes from 'prop-types'

const PokedexSprite = ({PokemonSprite}) => {
    return(
        <div>
            <img src={PokemonSprite} alt="Smiley face" height="150" width="150"></img>
        </div>
    )
};

PokedexSprite.propTypes = {
    PokemonSprite: PropTypes.string.isRequired,
};

export default PokedexSprite;