import React, {Component} from 'react';
import logo from './logo.png';
import PokedexGeneral from './components/Pokedex/'
import './App.css';

class App extends Component{
  render(){
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} alt="logo" height="25%" width="25%"/>
          <PokedexGeneral></PokedexGeneral>
        </header>
      </div>
    );
  }
}

export default App;